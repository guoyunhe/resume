document.addEventListener('DOMContentLoaded', function () {
  let lang = 'en';

  navigator.languages.map(function (l) {
    if (l.indexOf('zh') === 0) {
      //lang = 'zh'
    }
  });

  document.documentElement.lang = lang;

  new fullpage('#fullpage', {
    licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE'
  });
});
